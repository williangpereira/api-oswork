package br.com.osworks.services;

import br.com.osworks.exception.ClienteException;
import br.com.osworks.models.Cliente;
import br.com.osworks.repositories.ClienteRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepositories clienteRepositories;

    public Cliente salvar(Cliente cliente){

        Cliente clienteExistente = clienteRepositories.findByEmail(cliente.getEmail());

        if(clienteExistente!= null && clienteExistente.equals(cliente)){
            throw new ClienteException("Já existe um cliente cadastrado com este e-mail");
        }

        return clienteRepositories.save(cliente);
    }

    public void excluir(Long id){
        clienteRepositories.deleteById(id);
    }



}
